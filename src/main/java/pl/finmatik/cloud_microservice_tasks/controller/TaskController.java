package pl.finmatik.cloud_microservice_tasks.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.finmatik.cloud_microservice_tasks.model.Task;
import pl.finmatik.cloud_microservice_tasks.model.TaskDto;
import pl.finmatik.cloud_microservice_tasks.service.TaskService;

import java.util.List;

@RestController
@RequestMapping(path = "/task/")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/{id}")
    public Task getById(@PathVariable(name = "id") Long id) {
        return taskService.getOne(id);
    }

    @GetMapping("/owner/{id}")
    public List<Task> getByOwner(@PathVariable(name = "id") Long id) {
        return taskService.getByUser(id);
    }

    @PutMapping("/")
    public Long create(@RequestBody TaskDto taskDto) {
        return taskService.add(taskDto);
    }
}
