package pl.finmatik.cloud_microservice_tasks.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.finmatik.cloud_microservice_tasks.apiclient.UserApiClient;
import pl.finmatik.cloud_microservice_tasks.model.AppUserDto;
import pl.finmatik.cloud_microservice_tasks.model.Task;
import pl.finmatik.cloud_microservice_tasks.model.TaskDto;
import pl.finmatik.cloud_microservice_tasks.repository.TaskRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserApiClient userApiClient;

    public Long add(TaskDto taskDto) {
        Task newlyCreated = TaskDto.fromDto(taskDto);

        try {
            AppUserDto dto = userApiClient.get(taskDto.getOwnerId());

            newlyCreated = taskRepository.save(newlyCreated);

            return newlyCreated.getId();
        } catch (Exception e) {

        }
        throw new EntityNotFoundException("User owner can`t be found");
    }

    public List<Task> getByUser(long ownerId) {
        List<Task> tasks = taskRepository.findAllByOwnerId(ownerId);
        return tasks;
    }

    public Task getOne(long taskId) {
        Optional<Task> tasksOpt = taskRepository.findById(taskId);
        return tasksOpt.orElseThrow(() -> new EntityNotFoundException());
    }


}
