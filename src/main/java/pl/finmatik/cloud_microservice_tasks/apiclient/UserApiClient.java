package pl.finmatik.cloud_microservice_tasks.apiclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.finmatik.cloud_microservice_tasks.model.AppUserDto;

@FeignClient
public interface UserApiClient {
    @GetMapping("/{id}")
    public AppUserDto get(Long id);
}
