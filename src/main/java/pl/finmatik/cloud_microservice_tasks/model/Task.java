package pl.finmatik.cloud_microservice_tasks.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.LocalDateTime;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long ownerId;

    private String description;

    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:ss")
    private LocalDateTime created;

    private boolean done;

    public Task(Long ownerId, String description) {
        this.ownerId = ownerId;
        this.description = description;
    }
}

