package pl.finmatik.cloud_microservice_tasks.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {
    private Long ownerId;

    private String description;

    public static Task fromDto(TaskDto taskDto) {
        return new Task(taskDto.getOwnerId(), taskDto.getDescription());
    }
}
