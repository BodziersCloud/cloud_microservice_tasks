package pl.finmatik.cloud_microservice_tasks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.finmatik.cloud_microservice_tasks.model.Task;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByOwnerId(long ownerId);
}
